# HybridLPA

**HybridLPA** is a modified Label Propagation Algorithm for Metagenome read clustering. The algorithm is an extension of the Apache Spark-based version: **SpaRC** (refer to [paper](https://academic.oup.com/bioinformatics/article/35/5/760/5078476) and [sources](https://bitbucket.org/LizhenShi/sparc/src/master/)). HybridLPA leverages long-read sequencing platforms such as Pacific BioSciences (PacBio) and Oxford Nanopore Technologies (ONT) to improve short-read based metagenome read clustering. Hybrid LPA requries both short-read and long-read data. Here we provided two implementions, one in MPI and one in UPC++.




## Installation


After cloning the repo, run the following:

```
    cd HybridLPA && git submodule update --init --recursive
```
MPI and UPC++ versions are independent of each other, you may build one version that fits your environment.


### MPI Version

#### Requirements
* Linux 
* mpi >= 3.0
* c++ standard >= 11 


#### Build
```
    mkdir build && cd build
    cmake -DBUILD_SPARC_MPI=ON ..
    make sparc_mpi
```
### UPC++ Version

#### Requirements
* Linux 
* mpi >= 3.0
* c++ standard >= 11 
* upcxx 2020.3.2


#### Build

Install UPC++ is beyond this scope. 
Set environment *UPCXX_INSTALL* helps cmake to find upcxx.
Also remember set UPCXX_NETWORK to your network, otherwise smp is most likely be used. 
```
    mkdir build && cd build
    cmake -DBUILD_SPARC_UPCXX=ON ..
    make sparc_upcxx
```


## Usages

## Constructing a read gragh

The HybridLPA is designed for partitioning metagenome read graphs. A metagenome read graph could be constructed in one of the following two ways: 1) Apache Spark based: using [SpaRC] (https://bitbucket.org/LizhenShi/sparc/src/master/README.md); 2) MPI/UPC-based: as described below. Examples of using MPI are provided, UPC version should be very similar. Skip to the clustering step if you have already obtained a read gragh using SpaRC.

When running the following programs, please make sure there are enough nodes to hold all data in memory. Although some programs support storing temporary data in disk, it will make the progress slow.


### Kmer Counting

Find the kmer counting profile of the data, so that we decides how to filter out "bad" kmers. In this step we run edge_generating_$SURFIX where $SURFIX means mrmpi, mimir, mpi or upcxx. 

For example for mpi version:
```
    $./kmer_counting_mpi -h
    
    -h, --help
    shows this help message
    -i, --input
    input folder which contains read sequences
    -p, --port
    port number
    -z, --zip
    zip output files
    -k, --kmer-length
    length of kmer
    -o, --output
    output folder
    --without-canonical-kmer
    do not use canonical kmer

```


### Kmer-Reads-Mapping

Find shared reads for kmers with kmer_read_mapping_$SURFIX where $SURFIX means mrmpi, mimir, mpi or upcxx. 

For example for mpi version:
```
    $./kmer_read_mapping_mpi -h
    
    -h, --help
    shows this help message
    -i, --input
    input folder which contains read sequences
    -p, --port
    port number
    -z, --zip
    zip output files
    -k, --kmer-length
    length of kmer
    -o, --output
    output folder
    --without-canonical-kmer
    do not use canonical kmer

```

### Edge Generating 

Generate graph edges using edge_generating_$SURFIX where $SURFIX means mrmpi, mimir, mpi or upcxx. 

For example for mpi version:
```
    $./edge_generating_mpi -h
    
    -h, --help
    shows this help message
    -i, --input
    input folder which contains read sequences
    -p, --port
    port number
    -z, --zip
    zip output files
    -o, --output
    output folder
    --max-degree
    max_degree of a node; max_degree should be greater than 1
    --min-shared-kmers
    minimum number of kmers that two reads share. (note: this option does not work)
    
```


### Clustering

Forming clusters using HybridLPA on the mixture of long and short reads. 
In this step there are two options: PowerGraph LPA or *lpav1_mpi/upcxx*. 

*lpav1_mpi/upcxx* is our implementation of LPA in MPI or UPC++, which is build in the corresponding targets.

PowerGraph(https://github.com/jegonzal/PowerGraph) also provide LPA program which can be got from github.

*Remark:* Since this step is a graph clustering step, in theory any graph clustering algorithms can be used providing it can work on graphs of billions edges.



## Example Runs

Please find sbatch scripts of sample runs on [LAWRENCIUM](https://sites.google.com/a/lbl.gov/high-performance-computing-services-group/lbnl-supercluster/lawrencium) in [misc/example](misc/example) folder.


